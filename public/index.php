<?php
/**
 * Created by PhpStorm.
 * User: xu.gao
 * Date: 2016/6/7
 * Time: 11:46
 */

define('ROOT',__DIR__);

date_default_timezone_set('Asia/shanghai');

use Simple\Application;

require __DIR__.'/../bootstrap/authload.php';

require __DIR__.'/../config/config.php';

//启动核心文件

(new Application())->run($config);




