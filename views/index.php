<!DOCTYPE html>
<html>
<head>
    <title>Simple Framework</title>
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 85px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">

        <form action="/home/index/test" method="post" >

            姓名<input type="text" name="username" /><br>

            年龄<input type="text" name="age"><br>

            <button type="submit">提交</button>


        </form>

    </div>
</div>
</body>
</html>