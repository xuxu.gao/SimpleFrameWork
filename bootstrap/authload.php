<?php
/**
 * Created by PhpStorm.
 * User: xu.gao
 * Date: 2016/6/7
 * Time: 14:30
 */
require __DIR__.'/../vendor/autoload.php';

require __DIR__.'/../config/config.php';

//配置自动注入对象
\Simple\Web\Container::createObj("userService",'App\services\UserServiceImpl');