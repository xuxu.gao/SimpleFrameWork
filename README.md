#SimpleFrameWork

###**核心**

composer支撑框架的包管理。

###**路由**

1.支持2种自由方式

  (1)QUERY_STRING模式 eg:http://xxx.com/home/index/index
  
  (2)PathInfo模式 eg:http://xxx.com?m=home&c=index&a=index
  
   路由自动处理index.php，消除服务器配置rewrite的烦恼，也为不会配置rewrite的小白提供福音。
   
### 响应处理   

1.针对请求的多种格式进行封装Request和Response完全满足开发中遇到的操作。
    
    (1)针对发送请求的post数据做全面的情况处理，高效的php://input方式作为首选，以及额外的HTTP_RAW_POST_DATA都进行的情况判断。
    (2)针对get请求的参数做原始的REDIRECT_QUERY_STRING处理，提高效率。
    

### 实现IOC 

1.使用反射和注册树模式，使得对象管理更加方便高效。

  eg:
     //配置自动注入对象,一般写在bootstrap启动文件下面，也可以放在其他地方,require进来。
     
     \Simple\Web\Container::createObj("userService",'App\services\UserServiceImpl');
    
     控制器层读取
     public $userService;
         public function __construct()
         {
             parent::__construct();
             $this->userService = Container::getObj('userService');
         }
         
### AOP机制

1.Events机制更加方便。
  
  eg:
  
    Events::trigger(['App\services\UserServiceImpl','getBeforeAop',['name' => 'gaoxuxu']],
                          ['App\services\UserServiceImpl','getCurrentAop'],
                          ['App\services\UserServiceImpl','getAfterAop']
                      );
                      

  