<?php
/**
 * Created by PhpStorm.
 * User: xu.gao
 * Date: 2016/6/7
 * Time: 13:48
 */

$config =  [
            'route' => ['URL_MODE' =>1 ],//路由模式
            'defaultRoute' => 'Home/index/index',  //配置默认的路由
            'db' => [
                'mysql' => [
                    'dsn' => 'mysql:host=localhost;dbname=yiiadmin',
                    'username' => 'root',
                    'password' => '',
                    'charset' => 'utf8',
                ],

            ],

];