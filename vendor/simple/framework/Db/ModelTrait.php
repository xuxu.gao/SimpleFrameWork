<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/4/26
 * Time: 21:23
 */

namespace Simple\Db;


trait ModelTrait
{
    public $where;
    public $select;
    public function insert($sql,$params)
    {

        return (new Command())->execute_insert($sql,$params);

    }

    public function updated($sql,$params)
    {


    }
    public function deleted($sql,$params)
    {


    }
    public function where($condition)
    {

        $this->where[] = $condition;

        return $this;
    }
    /**
     * 多条件查询
     * @param $condition
     * @return $this
     */
    public function andWhere($condition)
    {
        if ($this->where === null) {

            $this->where[] = $condition;

        } else {
            $this->where[] = $condition;
        }
        return $this;
    }

    /**
     * select 查询列
     * @param $column
     */
    public function select($column)
    {
        $this->select = $column;
    }
}