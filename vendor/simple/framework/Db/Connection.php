<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/28
 * Time: 20:39
 */

namespace Simple\Db;
use Simple\Web\Container;

/**
 * 目前只支持PDO
 * Class Connection
 * @package Simple\Db
 */
class Connection
{

    public $dsn;

    public $username;

    public $password;

    public $pdo;

    public $attributes;

    /**
     * 初始化一些属性
     */
    public function init($params = [])
    {
        $this->dsn = isset($params['dsn']) ? $params['dsn'] : null;
        $this->username = isset($params['username']) ? $params['username'] : null;
        $this->password = isset($params['password']) ? $params['password'] : null;
        $this->attributes = isset($params['attributes']) ? $params['attributes'] : null;

        $this->open();
    }

    /**
     * 开启数据库的链接
     */
    public function open()
    {

       try{

           //判断常驻内存对象树上面已经存在的pdo连接对象
           $c_pdo = Container::getObj('pdo');
           if($c_pdo)
           {
               //直接获取存在的对象
               $this->pdo = $c_pdo;

           }else{

               //创建新对象
               $this->pdo = $this->createPdoInstance();

               Container::setObj('pdo',$this->pdo);
           }
           //设置PDO的一些属性
           $this->pdo->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);

        }catch(\PDOException $e){

           echo '数据库链接失败：错误信息'.$e->getMessage();
           exit();
        }
    }
    /**
     * 关闭数据库连接
     */
    public function close()
    {

        $this->pdo = null;
    }
    /**
     * 创建PDO实例
     */
    public function createPdoInstance()
    {

        return new \PDO($this->dsn,$this->username,$this->password,$this->attributes);

    }

}