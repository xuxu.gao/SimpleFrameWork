<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/28
 * Time: 23:03
 */

namespace Simple\Db;


use Simple\Web\Container;

class Query extends DB implements QueryInterface
{

    use QueryTrait;

    public $select;

    public $selectOption;

    public $from;

    public $join;

    public $groupBy;

    public $having;

    public $params = [];


    /**
     * 创建sql命令
     * @param null $db
     */
    public function createCommand($one_or_more = 1)
    {
        //组装sql语句
        $result = $this->build();
        $fetch_ret = (new Command())->executeCommand($result['sql'],$result['params']);
        $tmp_arr = [];
        if($fetch_ret)
        {
            $tmp_arr = $one_or_more == 1 ? $fetch_ret[0] : $fetch_ret;
        }
        return $tmp_arr;
    }
    /**
     * 生成sql语句
     *
     */
    public function build()
    {
        $select = $this->buildSelect();
        $from = $this->buildFrom();
        $join = $this->buildJoin();
        $where = $this->buildWhere();
        $group_by = $this->buildGroupBy();
        $having = $this->buildHaving();
        $order_by = $this->buildOrderBy();
        $limit = $this->buildLimit();
        $offset = $this->buildOffset();

        //拼装SQL
        $SQL = 'SELECT ';
        $SQL .= $select[0];
        $SQL .= ' FROM '.$from[0];
        //拼装 join
        if(!empty($join))
        {

            foreach ($join as $j)
            {

                $SQL .= ' '.$j[0];
                $SQL .= ' '.$j[1];
                $SQL .= ' ON '.$j[2];

            }

        }
        $tmp_bind = [];
        //拼装where 设置占位符
        if(!empty($where))
        {
            $i = 1;
            foreach ($where as $key =>  $w)
            {
                if($key == 0) {

                    $SQL .= ' WHERE '.key($w).' = '.' :val'.$i;

                }else{

                    $SQL .= ' AND '.key($w).' = '.' :val'.$i;
                }
                $tmp_bind = array_merge($tmp_bind,[':val'.$i => $w[key($w)]]);
                $i++;
            }
        }
        //拼装group_by
        if(!empty($group_by))
        {
            $g_val = [];
            foreach ($group_by as $g)
            {
                array_push($g_val,$g[0]);
            }

            $SQL .= '  GROUP BY '.implode(',',$g_val);
        }
        //拼装having
        if(!empty($having))
        {
            $SQL .= ' HAVING ';
            foreach ($having as $h)
            {

                $SQL .= $h[1].' '.$h[0].' '.$h[1].' AND ';

            }

            $SQL = rtrim($SQL,'AND ');
        }
        //拼装 order by
        if(!empty($order_by))
        {
            $SQL .= ' ORDER BY ';
            foreach ($order_by as $o)
            {

                $SQL .= $o[0].' '.$o[1].',';

            }

            $SQL = rtrim($SQL,',');

        }
        //拼装$limit ,offset
        if($limit)
        {

            $SQL .= ' LIMIT '.$limit;

            if($offset)
            {

                $SQL .= ' OFFSET '.$offset;
            }
        }

        return ['sql' => $SQL,'params' => $tmp_bind];
    }

    private function buildSelect()
    {

        $select = $this->select;

        return $select;
    }
    private function buildFrom()
    {

        $from = $this->from;

        return $from;
    }
    private function buildJoin()
    {
        $join = $this->join;

        return $join;
    }
    private function buildWhere()
    {
        $where = $this->where;

        return $where;
    }
    private function buildGroupBy()
    {
        $group_by = $this->groupBy;

        return $group_by;

    }
    private function buildHaving()
    {
        $having = $this->having;

        return $having;

    }
    private function buildOrderBy()
    {

        $order_by = $this->orderBy;

        return $order_by;

    }
    private function buildLimit()
    {
        $limit = $this->limit;

        return $limit;

    }
    private function buildOffset()
    {
        $offset = $this->offset;

        return $offset;
    }
    /**
     * 命令链的from方法
     * @param $tables
     * @return $this
     */
    public function from($tables)
    {
        if (!is_array($tables)) {

            $tables = preg_split('/\s*,\s*/', trim($tables), -1, PREG_SPLIT_NO_EMPTY);
        }
        $this->from = $tables;
        return $this;

    }

    /**
     * 内连
     * @param $table
     * @param string $on
     * @return $this
     */
    public function innerJoin($table, $on = '')
    {
        $this->join[] = ['INNER JOIN', $table, $on];

        return $this;
    }

    /**
     * 左连
     * @param $table
     * @param string $on
     * @return $this
     */
    public function leftJoin($table, $on = '')
    {
        $this->join[] = ['LEFT JOIN', $table, $on];

        return $this;

    }

    /**
     * 右连
     * @param $table
     * @param string $on
     */
    public function rightJoin($table, $on = '')
    {
        $this->join[] = ['RIGHT JOIN', $table, $on];

        return $this;
    }

    /**
     * 分组
     * @param $columns
     */
    public function groupBy($columns)
    {

        if (!is_array($columns)) {

            $columns = preg_split('/\s*,\s*/', trim($columns), -1, PREG_SPLIT_NO_EMPTY);
        }
        $this->groupBy[] = $columns;
        return $this;

    }

    /**
     * 多个分组
     * @param $columns
     * @return $this
     */
    public function andGroupBy($columns)
    {
        if (!is_array($columns)) {
            $columns = preg_split('/\s*,\s*/', trim($columns), -1, PREG_SPLIT_NO_EMPTY);
        }
        if ($this->groupBy === null) {

            $this->groupBy[] = $columns;

        } else {

            $this->groupBy[] = $columns;
        }
        return $this;

    }

    /**
     * have
     * @param $condition
     * @return $this
     */
    public function having($condition)
    {
        $this->having[] = $condition;

        return $this;

    }

    /**
     * 多个have
     * @param $condition
     * @return $this
     */
    public function andHaving($condition)
    {
        if ($this->having === null) {

            $this->having[] = $condition;

        } else {

            $this->having[] = $condition;
        }

        return $this;
    }

    /**
     * 查询字段
     * @param $columns
     * @param null $option
     * @return $this
     */
    public function select($columns, $option = null)
    {
        if (!is_array($columns)) {
            $columns = preg_split('/\s*,\s*/', trim($columns), -1, PREG_SPLIT_NO_EMPTY);
        }
        $this->select = $columns;
        $this->selectOption = $option;
        return $this;
    }

    /**
     * 查询全部
     */
    public function all()
    {
        return $this->createCommand(2);
    }

    /**
     * 查询单条
     * @return $this|Query
     */
    public function one()
    {
        return $this->createCommand(1);
    }

    /**
     * 统计数量
     * @param string $q
     */
    public function count($q = '*')
    {

    }
}