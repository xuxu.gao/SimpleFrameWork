<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/28
 * Time: 22:44
 */

namespace Simple\Db;


interface QueryInterface
{


    public function all();

    public function one();

    public function count($q = '*');

    public function where($condition);

    public function andWhere($condition);

    public function orWhere($condition);

    public function orderBy($columns);

    public function limit($limit);

    public function offset($offset);

}