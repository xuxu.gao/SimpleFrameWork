<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/4/18
 * Time: 20:43
 */

namespace Simple\Db;


use Simple\Web\Container;

class DB
{

    protected $transactionCounter = 0;
    public static $pdo = null;

    public function __construct()
    {
        self::$pdo = Container::getObj('pdo');
    }

    public function beginTransaction()
    {
        if(!$this->transactionCounter++)

            return  self::$pdo->beginTransaction();

        return $this->transactionCounter >= 0;
    }

    public function commit()
    {
        if(!--$this->transactionCounter)

            return self::$pdo->commit();

        return $this->transactionCounter >= 0;
    }

    public function rollback()
    {
        if($this->transactionCounter >= 0)
        {
            $this->transactionCounter = 0;

            return self::$pdo->rollback();
        }

        $this->transactionCounter = 0;
        return false;
    }


}