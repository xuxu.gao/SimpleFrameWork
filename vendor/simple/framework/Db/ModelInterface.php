<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/4/26
 * Time: 21:22
 */

namespace Simple\Db;


interface ModelInterface
{

    public function insert($sql,$params);

    public function updated($sql,$params);

    public function deleted($sql,$params);

    public function where($condition);

    public function andWhere($condition);

    public function select($columns);
}