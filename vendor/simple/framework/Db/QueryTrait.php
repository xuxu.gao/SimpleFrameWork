<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/28
 * Time: 22:38
 */

namespace Simple\Db;


trait QueryTrait
{


    public $where;

    public $limit = 0;

    public $offset = 0;

    public $orderBy;

    /**
     * 条件查询
     * @param $condition
     * @return $this
     */
    public function where($condition)
    {
        $this->where[] = $condition;

        return $this;
    }

    /**
     * 多条件查询
     * @param $condition
     * @return $this
     */
    public function andWhere($condition)
    {
        if ($this->where === null) {

            $this->where[] = $condition;

        } else {
            $this->where[] = $condition;
        }
        return $this;
    }

    /**
     * or 条件
     * @param $condition
     * @return $this
     */
    public function orWhere($condition)
    {
        if ($this->where === null) {

            $this->where[] = $condition;

        } else {
            $this->where[] = $condition;
        }
        return $this;
    }

    /**
     * 排序
     * @param $columns
     * @return $this
     */
    public function orderBy($columns)
    {
        $this->orderBy[] = $columns;
        return $this;
    }

    /**
     * 多个排序
     * @param $columns
     * @return $this
     */
    public function andOrderBy($columns)
    {
        if ($this->orderBy === null) {

            $this->orderBy[] = $columns;

        } else {

            $this->orderBy[] = $columns;
        }
        return $this;
    }
    /**
     * 几条记录 为分页查询使用一般
     * @param $limit
     * @return $this
     */
    public function limit($limit)
    {
        $this->limit = $limit;
        return $this;
    }
    /**
     * 跳过几条记录 为分页查询使用一般
     * @param $limit
     * @return $this
     */
    public function offset($offset)
    {
        $this->offset = $offset;
        return $this;
    }


}