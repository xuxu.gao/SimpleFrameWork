<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/28
 * Time: 23:13
 */

namespace Simple\Db;
use Simple\Web\Container;

/**
 * ORM的模型
 * Class Model
 * @package Simple\Db
 */
class Model implements ModelInterface
{
    use ModelTrait;

    private $is_array = false;

    public function __construct()
    {


    }
    /**
     * 返回model实例的方法
     */
    public static function find()
    {
        Container::createObj(get_called_class(),get_called_class());

        return Container::getObj(get_called_class());
    }
    /**
     * 存储方法
     * @throws \Exception
     */
    public function save()
    {
        //获取对象设置的变量，比对设置的model属性是否和数据库的对应
        $attrs = get_object_vars($this);
        $columns = $this->attributes();
        //对比属性 ,过滤不存在的属性
        $cattrs = $this->compareAttributes($attrs,$columns);
        $sql = $this->buildInsertSql($cattrs);
        $values = array_values($cattrs);
        return $this->insert($sql,$values);
    }
    /**
     * 删除方法
     */
    public function delete()
    {


    }

    /**
     * 单个对象的查询
     * @return mixed
     * @throws \Exception
     */
    public function one()
    {
        $tableName = $this->getTableName(get_called_class());

        $ret = $this->buildQuerySql($tableName);

        $obj = (new Command())->executeCommand($ret['SQL'],$ret['params']);

        $model = get_called_class();

        $tmp_obj = $this->is_array ? $obj[0] : self::array2obj($obj[0],new $model);


        return $tmp_obj;
    }

    /**
     * 转为数组
     */
    public function toArray()
    {
        $this->is_array = true;

        return Container::getObj(get_called_class());
    }

    /**
     * 回收一些垃圾属性
     */
    public function unset_garbage()
    {

        unset($this->where);
        unset($this->select);
        unset($this->is_array);
    }

    /**
     * 查询多个
     */
    public function all()
    {


    }
    /**
     * 组装 模型的sql语句
     */
    public function buildQuerySql($tableName = null)
    {

        $SQL = 'SELECT ';
        if($this->select){

            $SQL .= $this->select.' ';
        }else{

            $SQL .= ' * ';
        }


        $SQL .= ' FROM '.$tableName;
        //拼装where 设置占位符
        $tmp_bind = [];
        if(!empty($this->where))
        {
            $i = 1;
            foreach ($this->where as $key =>  $w)
            {
                if($key == 0) {

                    $SQL .= ' WHERE '.key($w).' = '.' :val'.$i;

                }else{

                    $SQL .= ' AND '.key($w).' = '.' :val'.$i;
                }
                $tmp_bind = array_merge($tmp_bind,[':val'.$i => $w[key($w)]]);
                $i++;
            }
        }
        return ['SQL' => $SQL,'params' => $tmp_bind];
    }
    /**
     * 获取数据库表的属性
     * @return array
     * @throws \Exception
     */
    public function attributes()
    {

        $tableName = $this->getTableName(get_called_class());

        $columns = $this->getColumns($tableName);

        return $columns;
    }

    /**
     * 查询数据库表列
     * @param null $table
     * @return array
     */
    public  function getColumns($table = null)
    {
        $columns = (new Command())->executeCommand(' SHOW FULL COLUMNS FROM '.$table);

        return array_column($columns,'Field');
    }
    /**
     * 比对 对象设置的  属性是否和table的属性一致
     * 过滤不存在的属性
     */
    public function compareAttributes($attr = [],$columns = [])
    {
        $tmp = [];
        foreach($attr as $key => $item)
        {
            if(in_array($key,$columns))
            {
                $tmp[$key] = $item;
            }
        }

        return $tmp;
    }
    /**
     * 组装插入的sql语句
     */
    public function buildInsertSql($attrs)
    {
        //拼接insert sql语句

        $tableName = $this->getTableName(get_called_class());

        $keys = array_keys($attrs);

        $tmp = [];
        foreach ($keys as $item)
        {
            array_push($tmp,'?');
        }
        $condins = implode(',',$tmp);
        $SQL = "INSERT INTO " . $tableName . " (" . implode(",",$keys) . ") VALUES ({$condins})";
        return $SQL;
    }

    /**
     * 获取table 名字
     * @param $class
     * @return mixed
     * @throws \Exception
     */
    public function getTableName($class)
    {
        $reflectionClass = new \ReflectionClass($class);

        $instance = $reflectionClass->newInstance();
        if(! method_exists($instance, 'tableName'))
        {
            throw new \Exception(get_called_class().' 未定义的方法：' . 'tableName');
        }
        $reflectionMethod = $reflectionClass->getMethod('tableName');
        //调用方法
        $tableName = $reflectionMethod->invoke($instance);

        return $tableName;
    }
    /**
     * array to obj
     */
    public  function array2obj($array,$obj)
    {

        foreach ($array as $key => $item)
        {
            $obj->$key = $item;

        }
        $obj->unset_garbage();
        return $obj;
    }

}