<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/30
 * Time: 21:06
 */

namespace Simple\Db;


use Simple\Web\Container;
use PDO;
class Command extends DB
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *执行sql语句，返回结果
     */
    public function executeCommand($sql,$params = [],$is_obj = false)
    {
        parent::beginTransaction();
        //执行sql语句
        $sth =  DB::$pdo->prepare($sql);
        $sth->execute($params);
        //使用关联数组的方式列出数据
        $result = $sth->fetchAll($is_obj ? PDO::FETCH_OBJ : PDO::FETCH_ASSOC);
        $sth->closeCursor();
        parent::commit();
        return $result;
    }
    /**
     * 执行insert
     * $sql sql语句
     * $params value值数组
     * $lastIsertId是否返回插入的id (默认返回影响的行数)
     */
    public function execute_insert($sql,$params,$lastIsertId = false)
    {
        parent::beginTransaction();
        $sth =  DB::$pdo->prepare($sql);

        $sth->execute($params);

        parent::commit();
        if($lastIsertId){

            $id =  DB::$pdo->lastInsertId(); ;
        }else{

            $id = $sth->rowCount();
        }
        return $id;
    }
    /**
     * 执行update
     */
    public function execute_update($sql,$params)
    {

        parent::beginTransaction();
        $sth =  DB::$pdo->prepare($sql);

        $sth->execute($params);
        parent::commit();

        $id = $sth->rowCount();
        return $id;
    }
    /**
     * 执行delete
     */
    public function execute_delete($sql,$params)
    {



    }


}