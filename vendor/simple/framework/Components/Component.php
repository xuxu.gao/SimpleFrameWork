<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/16
 * Time: 21:13
 */

namespace Simple\Components;


class Component
{

    public $SMARTY_TMPDIR   = '/views/';

    public $SMARTY_CONFIG   = '/views/tpl_conf/';

    public $SMARTY_CACHEDIR = '/views/cache/';

    public $SMARTY_COMPILE  = '/views/webdata/tpl_compile/';

    public $LIFTTIME        = 1800;

    public $SMARTY_DLEFT    = '<{';

    public $SMARTY_DRIGHT   = '}>';

    protected static $_instance = NULL;

    public function __construct()
    {
        if(is_null(self::$_instance))
        {
            $smarty = new \Smarty();
            self::$_instance = $smarty;
        }
        self::$_instance->setTemplateDir(ROOT.'\\..\\'.str_replace('/','\\',$this->SMARTY_TMPDIR));
        self::$_instance->setCompileDir(ROOT.'\\..\\'.str_replace('/','\\',$this->SMARTY_COMPILE));
        self::$_instance->setConfigDir(ROOT.'\\..\\'.str_replace('/','\\',$this->SMARTY_CONFIG));
        self::$_instance->compile_check    = true;
        self::$_instance->caching          = 1;
        self::$_instance->setCacheDir(ROOT.'\\..\\'.str_replace('/','\\',$this->SMARTY_CACHEDIR));
        self::$_instance->left_delimiter   = $this->SMARTY_DLEFT;
        self::$_instance->right_delimiter  = $this->SMARTY_DRIGHT;
        self::$_instance->cache_lifetime   = $this->LIFTTIME;
    }

    /**
     * 页面跳转
     *
     */
    public function render($path = '',$data = '')
    {
        //前台赋值
        if(is_array($data))
        {
            foreach ($data as $k => $v)
            {
                self::$_instance->assign($k,$v);
            }
        }
        //页面展示
        self::$_instance->display($path.'.php');
    }


}