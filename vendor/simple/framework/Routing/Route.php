<?php
/**
 * Created by PhpStorm.
 * User: xu.gao
 * Date: 2016/6/7
 * Time: 11:31
 */
namespace Simple\Routing;

class Route {

    public static  $url_mode;
    private static  $var_controller;
    private static  $var_action;
    private static  $var_module;

    /**
     * 初始化方法
     * @param type $config
     */
    public static function init($config) {

        self::$url_mode = isset($config['URL_MODE']) ? $config['URL_MODE'] : 0;
        self::$var_controller = isset($config['VAR_CONTROLLER']) ? $config['VAR_CONTROLLER'] : '';
        self::$var_action = isset($config['VAR_ACTION']) ? $config['VAR_ACTION'] : '';
        self::$var_module = isset($config['VAR_MODULE']) ? $config['VAR_MODULE'] : '';
    }

    /**
     * 获取url打包参数
     * @return type
     */
    public static  function makeUrl($defaultRoute) {

        $pathParam = '';

        switch (self::$url_mode) {
            //动态url传参 模式
            case 0:
                $pathParam = self::getParamByDynamic();
                break;
            //pathinfo 模式
            case 1:
                $pathParam = self::getParamByPathinfo();
                break;
            default:
                $pathParam = "";
        }
        if(!$pathParam['module'] && !$pathParam['controller'] && !$pathParam['action']) {

            $pathParam = self::getParamByPathinfo($defaultRoute);
        }
        return $pathParam;
    }

    /**
     * 获取参数通过url传参模式
     */
    public static function getParamByDynamic() {
        $arr = empty($_SERVER['QUERY_STRING']) ? array() : explode('&', $_SERVER['QUERY_STRING']);
        $data = array(
            'module' => '',
            'controller' => '',
            'action' => '',
            'param' => array()
        );
        if (!empty($arr)) {
            $tmp = array();
            $part = array();
            foreach ($arr as $v) {
                $tmp = explode('=', $v);
                $tmp[1] = isset($tmp[1]) ? trim($tmp[1]) : '';
                $part[$tmp[0]] = $tmp[1];
            }
            if (isset($part['m'])) {
                $data['module'] = $part['m'];
                unset($part[self::$var_module]);
            }
            if (isset($part['c'])) {
                $data['controller'] = $part['c'];
                unset($part[self::$var_controller]);
            }
            if (isset($part['a'])) {
                $data['action'] = $part['a'];
                unset($part[self::$var_action]);
            }
            switch ($_SERVER['REQUEST_METHOD']) {
                case 'GET':
                    $data['param'] = $part;
                    unset($_GET);
                    break;
            }
        }
        return $data;
    }

    /**
     * 获取参数通过pathinfo模式
     */
    public static  function getParamByPathinfo($path = null) {
        //去除/index.php的存在,以免干扰处理
        $part = $path ? explode('/',$path) : explode('/', trim(str_replace('/index.php','',$_SERVER['REQUEST_URI']), '/'));
        $data = array(
            'module' => '',
            'controller' => '',
            'action' => '',
            'param' => array()
        );
        if (!empty($part)) {
            krsort($part);
            $data['module'] = array_pop($part);
            $data['controller'] = array_pop($part);
            $data['action'] = array_pop($part);
            ksort($part);
            $part = array_values($part);
            if(!$path){

                switch ($_SERVER['REQUEST_METHOD']) {
                    case 'GET':
                        unset($_GET[self::$var_controller], $_GET[self::$var_action]);
                        $data['param'] = $part;
                        unset($_GET);
                        break;
                }
            }

        }
        return $data;
    }

}