<?php
/**
 * Created by PhpStorm.
 * User: xu.gao
 * Date: 2016/6/8
 * Time: 11:34
 */

namespace Simple\Routing;


class Url {

    /**
     * 获取url传递到额参数
     */
    public static function paramData()
    {
       return Route::makeUrl()['param'];
    }

}