<?php
/**
 * Created by PhpStorm.
 * User: xu.gao
 * Date: 2016/6/8
 * Time: 15:50
 */

namespace Simple\Web;

/**
 * 对象容器
 * Class Container
 * @package Simple\Web
 */
class Container {

    /**
     * 存储对象的数组树
     * @var array
     */
    private static $_singletons = [];

    /**
     * 获取一个对象
     * @param $alias
     * @return mixed
     */
    public static function getObj($alias)
    {
        return isset(self::$_singletons[$alias]) ? self::$_singletons[$alias] : null;
    }

    /**
     * 用于设置一些常驻对象
     * @param $alias
     * @param $instance
     */
    public static function setObj($alias,$instance)
    {
         self::$_singletons[$alias] = $instance;
    }
    /**
     * 创建对象到对象树
     * @param $alias
     * @param $param1
     * @param $param2
     */
    public static function createObj($alias,$param1 = null,$options = [])
    {

        $reflectionClass  = new \ReflectionClass($param1);
        //利用反射创建实例对象
        $instance = $reflectionClass->newInstance();
        //存入对象数组
        self::$_singletons[$alias] = $instance;

    }
    /**
     * 销毁一个对象
     * @param $alias
     */
    public static function _unset($alias)
    {

        unset(self::$_singletons[$alias]);
    }

}