<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/24
 * Time: 23:16
 */

namespace Simple\Web;


class Json
{
    /**
     * JSON转换为数组
     * @param $json
     * @param bool $asArray
     * @return mixed
     * @throws \Exception
     */
    public static function decode($json, $asArray = true)
    {
        if (is_array($json)) {

            throw new \Exception('不可用的JSON数据.');
        }
        $decode = json_decode((string) $json, $asArray);

        switch (json_last_error()) {

            case JSON_ERROR_NONE:
                break;
            case JSON_ERROR_DEPTH:
                throw new \Exception('已超出最大堆栈深度.');
            case JSON_ERROR_CTRL_CHAR:
                throw new \Exception('控制字符错误，可能错误编码.');
            case JSON_ERROR_SYNTAX:
                throw new \Exception('语法错误.');
            case JSON_ERROR_STATE_MISMATCH:
                throw new \Exception('无效或错误的JSON.');
            case JSON_ERROR_UTF8:
                throw new \Exception('可能错误UTF-8编码.');
            default:
                throw new \Exception('未知JSON解码错误.');
        }
        return $decode;
    }

    /**
     * JSON_UNESCAPED_UNICODE //中文不转为unicode
     * JSON_UNESCAPED_SLASHES //不转义反斜杠
     * JSON_UNESCAPED_UNICODE(256) + JSON_UNESCAPED_SLASHES(64) = 320
     * @param $value
     * @param int $options
     */
    public static function encode($value, $options = 320)
    {

        $value = static::processData($value,$options);

        $json = json_encode($value, $options);

        return $json;
    }
    /**
     * 转换成完美的数组
     * @param $data
     * @param int $options
     * @return array|\stdClass
     */
    private static function processData($data, $options = 0)
    {
        if (is_object($data)) {

                $result = [];

                foreach ($data as $name => $value) {

                    $result[$name] = $value;
                }

                $data = $result;
            }
            if ($data === []) {

                return new \stdClass();
            }

        if (is_array($data)) {

            foreach ($data as $key => $value) {

                if (is_array($value) || is_object($value)) {

                    $data[$key] = static::processData($value, $options);
                }
            }
        }
        return $data;
    }


}