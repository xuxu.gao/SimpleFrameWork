<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/25
 * Time: 15:31
 */

namespace Simple\Web;


class Events
{


    public static function singleTrigger($current=[])
    {
        //当前方法
        if(!empty($current) && count($current) >= 2)
        {
            $reflectionClass  = new \ReflectionClass($current[0]);
            //利用反射创建实例对象
            $instance = $reflectionClass->newInstance();
            if(! method_exists($instance, $current[1]))
            {
                throw new \Exception('未定义的方法：' . $current[1]);
            }
            //执行current方法
            $parameter = isset($current[2]) ? $current[2] : null;
            $reflectionMethod = $reflectionClass->getMethod($current[1]);
            //调用方法
            $reflectionMethod->invoke($instance,$parameter);
        }
    }

    /**
     * @param array $current 当前
     * @param array $before 之前
     * @param array $after 之后
     */
    public static function trigger($before = [],$current=[],$after = [])
    {
        /**
         * 必须写类的命名空间
         * 执行方法
         * 参数可不传递
         */
        //判断是否添加执行前的方法
        if(!empty($before) && count($before) >= 2)
        {
            $reflectionClass  = new \ReflectionClass($before[0]);
            //利用反射创建实例对象
            $instance = $reflectionClass->newInstance();
            if(! method_exists($instance, $before[1]))
            {
                throw new \Exception('未定义的方法：' . $before[1]);
            }
            //执行before方法
            $parameter = isset($before[2]) ? $before[2] : null;
            $reflectionMethod = $reflectionClass->getMethod($before[1]);
            //调用方法
            $reflectionMethod->invoke($instance,$parameter);
        }
        //当前方法
        if(!empty($current) && count($current) >= 2)
        {
            $reflectionClass  = new \ReflectionClass($current[0]);
            //利用反射创建实例对象
            $instance = $reflectionClass->newInstance();
            if(! method_exists($instance, $current[1]))
            {
                throw new \Exception('未定义的方法：' . $current[1]);
            }
            //执行current方法
            $parameter = isset($current[2]) ? $current[2] : null;
            $reflectionMethod = $reflectionClass->getMethod($current[1]);
            //调用方法
            $reflectionMethod->invoke($instance,$parameter);
        }
        //after方法
        if(!empty($after) && count($after) >= 2)
        {
            $reflectionClass  = new \ReflectionClass($after[0]);
            //利用反射创建实例对象
            $instance = $reflectionClass->newInstance();
            if(! method_exists($instance, $after[1]))
            {
                throw new \Exception('未定义的方法：' . $after[1]);
            }
            //执行before方法
            $parameter = isset($after[2]) ? $after[2] : null;
            $reflectionMethod = $reflectionClass->getMethod($after[1]);
            //调用方法
            $reflectionMethod->invoke($instance,$parameter);
        }
    }

}