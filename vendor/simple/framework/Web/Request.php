<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/17
 * Time: 20:45
 */

namespace Simple\Web;

/**
 * 获取请求数据类
 * Class Request
 * @package Simple\Web
 */
class Request
{
    /**
     * 判断是否是https
     * $_SERVER['HTTPS'] on || off
     * 负载平衡器或反向代理支持http_x_forwarded_proto
     * @return bool
     */
    public function getIsSecureConnection()
    {
        return isset($_SERVER['HTTPS']) && (strcasecmp($_SERVER['HTTPS'], 'on') === 0 || $_SERVER['HTTPS'] == 1)
        || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && strcasecmp($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') === 0;
    }

    /**
     * 当前运行脚本所在的服务器的主机名
     * @return null
     */
    public function getServerName()
    {
        return isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : null;
    }

    /**
     * 获取客户端IP
     * @return null
     */
    public function getUserIP()
    {
        return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
    }

    /**
     * 获取加密端口
     * @return int
     */
    public function getSecurePort()
    {

        return $this->getIsSecureConnection() && isset($_SERVER['SERVER_PORT']) ? (int) $_SERVER['SERVER_PORT'] : 443;

    }

    /**
     * 获取正常端口
     * @return int
     */
    public function getPort()
    {

      return !$this->getIsSecureConnection() && isset($_SERVER['SERVER_PORT']) ? (int) $_SERVER['SERVER_PORT'] : 80;

    }

    /**
     * 获取Host信息
     * @return string
     */
    public function getHostInfo()
    {
        $_hostInfo = '';
        $secure = $this->getIsSecureConnection();
        $http = $secure ? 'https' : 'http';
        if (isset($_SERVER['HTTP_HOST'])) {

            $_hostInfo = $http . '://' . $_SERVER['HTTP_HOST'];

        } elseif (isset($_SERVER['SERVER_NAME'])) {

            $_hostInfo = $http . '://' . $_SERVER['SERVER_NAME'];

            $port = $secure ? $this->getSecurePort() : $this->getPort();

            if (($port !== 80 && !$secure) || ($port !== 443 && $secure)) {

                $_hostInfo .= ':' . $port;
            }
        }
        return $_hostInfo;
    }

    /**
     * 获取GET参数
     * @param null $name
     * @param null $defaultValue
     * @return mixed
     */
    public function get($name = null, $defaultValue = null)
    {
        if ($name === null) {
            return $this->getQueryParams();
        } else {
            return $this->getQueryParam($name, $defaultValue);
        }
    }

    /**
     * 获取单个GET值
     * @param $name
     * @param null $defaultValue
     * @return null
     */
    public function getQueryParam($name, $defaultValue = null)
    {
        $params = $this->getQueryParams();

        return isset($params[$name]) ? $params[$name] : $defaultValue;
    }

    /**
     * 获取整个GET数组
     * @return mixed
     */
    public function getQueryParams()
    {
        $vars = [];
        $input    = $_SERVER['REDIRECT_QUERY_STRING'];
        if(!empty($input)){
            $pairs    = explode("&", $input);
            foreach ($pairs     as $pair) {
                $nv                = explode("=", $pair);

                $name            = urldecode($nv[0]);
                $nameSanitize    = preg_replace('/([^\[]*)\[.*$/','$1',$name);
                $nameMatched    = str_replace('.','_',$nameSanitize);
                $nameMatched    = str_replace(' ','_',$nameMatched);

                $vars[$nameSanitize]    = $_REQUEST[$nameMatched];
            }
        }
        return $vars;
    }

    /**
     * 获取post数据
     * @param null $name
     * @param null $defaultValue
     */
    public function post($name = null, $defaultValue = null)
    {

        if ($name === null) {

            return $this->getPostParams();
        } else {

            return $this->getPostParam($name, $defaultValue);
        }
    }

    /**
     * 获取某个name的值
     * @param $name
     * @param null $defaultValue
     * @return null
     */
    public function getPostParam($name, $defaultValue = null)
    {

        $params = $this->getPostParams();

        return isset($params[$name]) ? $params[$name] : $defaultValue;
    }

    /**
     * 获取所有情况下的post数据
     * @return array
     */
    public function getPostParams()
    {
        /**
         * php://input 允许读取 POST 的原始数据。和 $HTTP_RAW_POST_DATA 比起来，它给内存带来的压力较小，
         * 并且不需要任何特殊的 php.ini 设置。php://input 不能用于 enctype="multipart/form-data"
         */
        $input = file_get_contents("php://input");
        $vars = [];
        if(!empty($input)){
            $pairs    = explode("&", $input);
            foreach ($pairs as $pair) {

                $nv              = explode("=", $pair);

                $name            = urldecode($nv[0]);
                $nameSanitize    = preg_replace('/([^\[]*)\[.*$/','$1',$name);

                $nameMatched     = str_replace('.','_',$nameSanitize);
                $nameMatched     = str_replace(' ','_',$nameMatched);

                $vars[$nameSanitize]    = $_REQUEST[$nameMatched];
            }
        }
        if(empty($vars)){

            /**
             * 接收Content-Type: application/x-www-form-urlencoded提交的数据
             */
            if($_POST){

                $vars = $_POST;

            }else{
                /**
                 * 接受未识别 MIME 类型的数据
                 * text/xml  soap
                 */
                $vars = @$GLOBALS['HTTP_RAW_POST_DATA'];
            }

        }
        return $vars;
    }
    /**
     * 生成csrf token
     */
    public function getCsrfToken()
    {

        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-.';

        $mask = substr(str_shuffle(str_repeat($chars, 5)), 0, 8);

        $this->_csrfToken = str_replace('+', '.', base64_encode($mask));

    }



}