<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/3/15
 * Time: 21:13
 */

namespace Simple;
use Simple\Routing\Route;
use Simple\Web\Events;

class Application
{

    public $version = '1.0';

    public function __construct()
    {

    }
    /**
     * 防止错误的方法调用
     * @param $name
     * @param $arguments
     */
    function __call($name, $arguments)
    {
        echo '不存在的方法',$name.'<br/>';
        echo '<pre>';
        echo print_r($arguments),'<br/>';
        exit(1);
    }
    /**
     * 防止错误的方法调用
     * @param $method
     * @param $arg
     */
    public static function __callStatic($method,$arg){

        echo '不存在的静态方法'.$method.'<br/>';
        echo '<pre>';
        echo print_r($arg),'<br/>';
        exit(1);
    }

    //启动方法
    public  function run($config)
    {
        Events::singleTrigger(['Simple\Db\Connection','init',$config['db']['mysql']]);
        Route::init($config['route']); //传入参数进行初始化
        $params = Route::makeUrl($config['defaultRoute']);    //makeUrl方法执行，完成参数打包
        $this->bootstrap($params);
    }

    /**
     * 根据留有调用控制器方法
     * @param array $params
     */
    private  function bootstrap($params = [])
    {
        try
        {

            $obj = '\\App\\controllers\\'.$params['module'].'\\'.ucfirst ($params['controller']).'Controller';
            //通过反射获取类
            $reflectionClass  = new \ReflectionClass($obj);
            //利用反射创建实例对象
            $instance = $reflectionClass->newInstance();
            $reflectionMethod = $reflectionClass->getMethod($params['action']);
            //调用方法
            $reflectionMethod->invoke($instance);

        }catch (\Exception $e){

            dump($e->getMessage().'  '.$e->getFile().'  '.$e->getLine());
        }

    }

    public function __set($name, $value)
    {

    }

    public function __get($name)
    {

    }

    /**
     * 打印对象的时候调用
     * toString
     * @return string
     */
    public function __toString()
    {

        return $this->version;
    }

    /**
     * 在类外部，isset()测试类的私有变量时会被调用
     * @param $name
     */
    public function __isset($name)
    {

    }

}