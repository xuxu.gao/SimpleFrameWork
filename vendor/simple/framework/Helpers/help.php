<?php
/**
 * Created by PhpStorm.
 * User: xu.gao
 * Date: 2016/6/7
 * Time: 14:27
 */

function dump($data)
{
    $func = is_array($data) || is_object($data) ? "print_r" : "var_dump";

    echo '<pre>';

    $func($data);

    exit;
}